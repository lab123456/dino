const score = document.getElementById("score");
const dino = document.getElementById("dinosaur");
const cactus = document.getElementById("cactus");

function jump() {

dino.classList.add("jump-animation");

setTimeout(() => 
dino.classList.remove("jump-animation"), 500);

}

document.addEventListener('keypress', (event) => {
    if (!dino.classList.contains("jump-animation"))
    jump();
})

setInterval(

   () => {
       const dinoTop = parseInt(window.getComputedStyle(dino).getPropertyValue("top"));
       const cactusLeft = parseInt(window.getComputedStyle(cactus).getPropertyValue("left"));
       score.innerText++

        if (cactusLeft<0) {
            cactus.style.display = 'none'
        } else {
            cactus.style.display = ''
        }

       if (cactusLeft<50 && cactusLeft>0 && dinoTop>150) {
           alert("Ти програв " + score.innerText + " зіграєш ще раз?");
        location.reload();           
       }
   }, 50
)

   

